package Lambdas;

@FunctionalInterface
public interface Countable {
    int count (int value1, int value2, int value3);
}
