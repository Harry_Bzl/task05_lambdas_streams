package Lambdas;

public class TestLambdas {
    public static void main(String[] args) {
        Countable max = (a,b,c)-> {if (a>=b && a>=c) return a;
                                   else if (b>=a && b>=c) return b;
                                   else return c;};

        int x = 2;
        int y = 5;
        int z = 23;
        System.out.println("Max value of " + x + ", " + y + " and "+ z + " is " + max.count(x,y,z));

        Countable average = (a, b, c)-> (a+b+c)/3;
        System.out.println("Average value of " + x + ", " + y + " and "+ z + " is " + average.count(x,y,z));

    }
}
