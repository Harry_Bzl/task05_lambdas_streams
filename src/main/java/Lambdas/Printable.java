package Lambdas;

@FunctionalInterface
public interface Printable {
    void print();
}
