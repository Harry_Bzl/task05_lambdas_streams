package Lambdas;

import java.util.Scanner;

public class Client {
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        String command;
        do {
            System.out.println("Enter the command: \n  Lambda | Method | Anonymous | Object");
            command = in.nextLine();
        }
        while (!command.equalsIgnoreCase("Lambda")&&!command.equalsIgnoreCase("Method") &&
                !command.equalsIgnoreCase("Anonymous")&&!command.equalsIgnoreCase("Object"));

        System.out.println("Enter a message: ");
        String message = in.nextLine();
        RemoteControl rc = new RemoteControl();
        Message mes = new Message(message);

        if (command.equalsIgnoreCase("Lambda")){
            rc.setMessage(()->System.out.println("Print message: " + message + " using lambda"));
            rc.printMessage();
        }
        else if (command.equalsIgnoreCase("Method")){
            rc.setMessage(mes::method);
            rc.printMessage();
        }
        else if (command.equalsIgnoreCase("Anonymous")){
            Client cl = new Client();
            cl.anonimousMessage(mes);
        }
        else {
            ObjectMessage om = new ObjectMessage(mes);
            rc.setMessage(om);
            rc.printMessage();
        }

    }

    private void anonimousMessage(Message message) {
        class AnonymousMessage implements Printable {
            private Message message;

            private AnonymousMessage(Message message){
                this.message = message;
            }
            @Override
            public void print() {
                message.anonymous();
            }
        }
        AnonymousMessage anonymousMessage = new AnonymousMessage(message);
        anonymousMessage.print();

    }


}
