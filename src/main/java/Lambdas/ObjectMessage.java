package Lambdas;

public class ObjectMessage implements Printable {
    Message message;

    public ObjectMessage(Message message){
        this.message = message;
    }
    @Override
    public void print() {
        message.object();
    }
}
