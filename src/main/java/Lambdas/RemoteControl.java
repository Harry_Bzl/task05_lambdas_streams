package Lambdas;

public class RemoteControl {
    private Printable message;

    public void setMessage (Printable message){
        this.message = message;
    }

    public void printMessage (){
        message.print ();
    }
}
