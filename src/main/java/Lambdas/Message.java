package Lambdas;

public class Message {
    private  String message;

    public Message (String message){
        this.message = message;
    }

    public void lambda () {
        System.out.println("Print message: " + message + " using lambda");
    }

    public void method () {
        System.out.println("Print message: " + message + " using message reference");
    }

    public void anonymous () {
        System.out.println("Print message: " + message + " using anonymous");
    }

    public void object () {
        System.out.println("Print message: " + message + " using object of command class");
    }

}
