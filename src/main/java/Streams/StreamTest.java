package Streams;

import java.util.*;

public class StreamTest {
    public static void main(String[] args) {
        StreamMethods methods = new StreamMethods();
        int numOfElements;

        do {
            numOfElements = insertNumber();
            if (numOfElements<1) {
                System.out.println("Put the number that is bigger than 0 " );
            }
        }
        while (numOfElements<1);

        // Generate stream using Stream.of
        methods.setCurrentValues( methods.generate1(numOfElements));
        System.out.println("Generate stream using Streame.of");
        methods.printStream();

        // Generate stream using Stream.iterate
        methods.setCurrentValues( methods.generate2(numOfElements));
        System.out.println("Generate stream using Streame.iterate");
        methods.printStream();


        // Generate stream using Stream.generate
        methods.setCurrentValues( methods.generate2(numOfElements));
        System.out.println("Generate stream using Streame.generate");
        methods.printStream();



        // max
        OptionalInt max = Arrays.stream(methods.getCurrentValues()).mapToInt(Integer::intValue).max();
        System.out.println("Max value is: " + max);
        //min
        OptionalInt min = Arrays.stream(methods.getCurrentValues()).mapToInt(Integer::intValue).min();
        System.out.println("Min value is: " + min);
        // sum
        int sum = Arrays.stream(methods.getCurrentValues()).mapToInt(Integer::intValue).sum();
        System.out.println("Sum using Stream.sum is: " + sum);
        System.out.println("Sum using Stream.reduce is: " + Arrays.stream(methods.getCurrentValues()).reduce((n,m)->n+m));
        //Count
        System.out.println("Count values, that are bigger than average " + sum/methods.getCurrentValues().length+" using Stream.count is: " +
                Arrays.stream(methods.getCurrentValues()).filter(n-> n>sum/methods.getCurrentValues().length).count());

    }


    private static int insertNumber() {
        Scanner input = new Scanner(System.in);
        boolean continueInput = true;
        int value = 0;
        do {
            System.out.println(" Enter the length of stream: ");
            try{
                value = input.nextInt();
                continueInput = false;
                System.out.println(
                        "The number entered is " + value);
            }
            catch (InputMismatchException ex) {
                System.out.println("Try again. (" +
                        "Incorrect input: an integer is required)");
                input.nextLine();
            }
        }
        while (continueInput);
        return value;
    }
}
