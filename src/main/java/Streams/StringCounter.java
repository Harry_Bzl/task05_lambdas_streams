package Streams;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Scanner;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.Stream;

class StringCounter {
    private List <String> textLines;
    private List <String> words;

    StringCounter(){
        textLines = new ArrayList<>();
        Scanner in = new Scanner(System.in);
        String word;
        do {
            System.out.println("Enter textLines. Click 'Enter' to add new word. Click 2 times to finish!");
             word = in.nextLine();
            if (!word.isEmpty()){
                textLines.add(word);
            }

        }
        while (!word.isEmpty());

        words = textLines.stream().flatMap(n-> Stream.of(n.split(" "))).collect(Collectors.toList());
    }

    void printLines(){
        System.out.println("Worlds are ");
        for (String i: textLines){

            System.out.println(i);
        }
    }

    int numberOfWords(){
        return textLines.size();
    }

    List <String> sortedWords(){
        return words.stream().sorted().collect(Collectors.toList());
    }

    Map<String,Long> groopWords(){
        return textLines.stream().flatMap(n-> Stream.of(n.split(" ")))
                .map(word -> word.replaceAll("[^a-zA-Z]", "").toLowerCase().trim())
                .filter(word -> !word.isEmpty())
                .collect(Collectors.groupingBy(Function.identity(), Collectors.counting()));
    }

    Map <Character, Long> countChars() {
        return textLines.stream().flatMap(n-> Stream.of(n.split(" ")))
                .flatMap(n-> n.chars().mapToObj(i->(char)i))
                .filter(n-> !Character.isUpperCase(n))
                .collect(Collectors.groupingBy(Function.identity(), Collectors.counting()));
    }


}
