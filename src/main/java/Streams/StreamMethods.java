package Streams;

import java.util.Arrays;
import java.util.List;
import java.util.Random;
import java.util.stream.Stream;

public class StreamMethods {
    private Integer [] currentValues;

    public Stream <Integer> generate1 (int numOfElements){
        Integer [] nums = new Integer[(int)numOfElements];
        Random rand = new Random();
        for (int i =0; i<numOfElements; i++){
            nums [i] = (Integer)rand.nextInt(100);
        }
        Stream <Integer> str = Stream.of(nums);
        return str;
    }

    public Stream <Integer> generate2 (int numOfElements){
        Random rnd = new Random();
        return Stream.iterate(1, n-> rnd.nextInt(100)).limit(numOfElements);
    }

    public Stream <Integer> generate3 (int numOfElements){
        Random rnd = new Random();
        return Stream.generate(()->rnd.nextInt(100)).limit(numOfElements);
    }

    public Integer[] getCurrentValues() {
        return currentValues;
    }

    public void setCurrentValues(Integer[] currentValues) {
        this.currentValues = currentValues;
    }
    public void setCurrentValues(Stream <Integer> currentValues) {

        this.currentValues = currentValues.toArray(Integer[]::new);
    }

    public void printStream () {
        System.out.println("Streame contains elements: ");
        Arrays.stream(currentValues).forEach(n-> System.out.print(n + ", "));
        System.out.println();
    }
}
