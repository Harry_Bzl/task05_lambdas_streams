package Streams;

import java.util.List;

public class StringCounterTest {
    public static void main(String[] args) {
        StringCounter sc = new StringCounter();
        sc.printLines();
        System.out.println("Number of textLines is " + sc.numberOfWords());
        System.out.println("List in sorted order is: ");
        List <String> list = sc.sortedWords();
        for (String i: list){
            System.out.print(i+ ", ");
        }
        System.out.println();
        System.out.println("Words counter: ");
        sc.groopWords().forEach((k,v)-> System.out.println(k + "---" + v));
        System.out.println("Occurrence number of each symbol except upper case characters");
        sc.countChars().forEach((k,v)-> System.out.println(k + "---" + v));
    }
}
